import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Dynamic title'
  arr = [1,2,3,4,5]

  obj = {
    a:1,
    b:{c:42}
  }

  img = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/1200px-Angular_full_color_logo.svg.png"

  constructor(){
    setTimeout(() => {
      console.log("timeout is over")
      this.img = 'https://i.pinimg.com/736x/96/fd/e2/96fde29a5306124a05af1afd74db8778.jpg'
    }, 5000)
  }



}
